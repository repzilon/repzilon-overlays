#! /bin/sh
#-------------------------------------------------------------------------
# Gentoo repository to Repzilon overlay import script
# Copyright (C) 2024 René Rhéaume <repzilon@users.noreply.gitlab.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------

#-------------------------------------------------------------------------
# Main module
#-------------------------------------------------------------------------
if [ $# -lt 2 ]; then
	echo "Usage: import.sh <atom> <overlay>" >&2
elif [ ! -d "$2" ]; then
	echo "Overlay directory $2 does not exist." >&2
else
	case $1 in
	*-*/*)
		mkdir -p "$2/$(dirname "$1")"
		cp -a "gentoo/$1" "$2/$(dirname "$1")"
		git commit "$2/$1" -a -m "In $2 : $1 import from gentoo"
		;;
	*)
		echo "$1 is not a Gentoo package atom." >&2
		;;
	esac
fi
